import multer from 'multer';

// Configuração de armazenamento e nomenclatura dos arquivos
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads'); // Diretório onde os arquivos serão armazenados (crie este diretório na raiz do seu projeto)
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`); // Nome do arquivo salvo no servidor
  }
});

// Opções de upload
const upload = multer({ storage });

export { upload };

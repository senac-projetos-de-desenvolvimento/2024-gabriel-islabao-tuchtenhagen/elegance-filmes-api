import { Router } from "express";
import { clienteCreate, clienteIndex, clienteLogin } from "./controllers/clienteController.js";
import { filmeCreate, filmeDelete, filmeDestaca, filmeDestaques, filmeIndex, filmeShow, filmePesquisa } from "./controllers/filmeController.js";
import { avaliacaoCreate, avaliacaoDestroy, avaliacaoFilme, avaliacaoGraphEstrelas, avaliacaoIndex, dadosGerais } from "./controllers/avaliacaoController.js";
import { adminIndex, adminCreate } from "./controllers/adminController.js";
import { loginAdmin } from "./controllers/adminLoginController.js";
import { chatbotCreate, chatbotIndex } from "./controllers/chatBotController.js";

const router = Router();

router.get('/clientes', clienteIndex)
      .post('/clientes', clienteCreate)
      .post('/login', clienteLogin);



router.get('/filmes', filmeIndex)
      .get('/filmes/destaques', filmeDestaques)
      .delete("/filmes/:id", filmeDelete)
      .post('/filmes', filmeCreate)
      .get('/filmes/:id', filmeShow)
      .patch('/filmes/destaca/:id', filmeDestaca)
      .get("/filmes/pesquisa/:palavra", filmePesquisa);
      
router.get('/avaliacoes', avaliacaoIndex)
      .post('/avaliacoes', avaliacaoCreate)
      .delete('/avaliacoes/:id', avaliacaoDestroy)
      .get('/avaliacoes/filmes/:id', avaliacaoFilme) // Corrigido
      .get('/avaliacoes/dados/estrelas', avaliacaoGraphEstrelas);
      
router.get('/dadosgerais', dadosGerais);

// Corrigido para /admins
router.get('/admins', adminIndex) 
      .post('/admins', adminCreate);

router.post("/loginAdmin", loginAdmin);

router.get('/api/chatbot', chatbotIndex)
      .post('/api/salvar-contato-chatbot', chatbotCreate)

export default router;
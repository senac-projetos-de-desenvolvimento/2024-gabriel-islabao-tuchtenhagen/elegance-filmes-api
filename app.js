import express from 'express';
import cors from 'cors';
import routes from './routes.js';
import { sequelize } from './databases/conecta.js';
import { Cliente } from './models/Cliente.js';
import { Filme } from './models/Filme.js';
import { Avaliacao } from './models/Avaliacao.js';
import { Admin } from './models/Admin.js';
import { ChatBotContato } from './models/ChatBotContato.js';
import path from 'path'; // Importando 'path' para manipulação de caminhos

const app = express();
const port = 3004;

// Substituindo __dirname com import.meta.url para compatibilidade com ES modules
const dirname = path.dirname(new URL(import.meta.url).pathname);

// Middleware
app.use(express.json());
app.use(cors());
app.use(routes);

// Serve arquivos estáticos da pasta build do front-end
app.use(express.static(path.join(dirname, 'client/build')));

async function conecta_db() {
  try {
    await sequelize.authenticate();
    console.log('Conexão com banco de dados realizada com sucesso');
    // Sincronizando as tabelas
    await Admin.sync({ force: true });
    console.log("Tabela de Admins: Ok");
    await Cliente.sync({ force: true });
    await ChatBotContato.sync({ force: true });
    await Filme.sync({ force: true });
    await Avaliacao.sync({ force: true });
  } catch (error) {
    console.error('Erro na conexão com o banco: ', error);
  }
}
conecta_db();

// Rota para gerar o link do WhatsApp
app.get('/api/send-whatsapp', (req, res) => {
  const phone = req.query.phone;

  if (!phone) {
    return res.status(400).json({ error: 'Número de telefone não fornecido.' });
  }

  const whatsappLink = `https://wa.me/${phone}`;
  res.redirect(whatsappLink);
});

// Rota para salvar os dados de contato dos clientes
app.post('/api/salvar-contato-chatbot', (req, res) => {
  const { nome, contato } = req.body;
  if (!nome || !contato) {
    return res.status(400).json({ msg: "Erro... Informe os dados" });
  }

  // Lógica para salvar os dados no banco ou em outro lugar
  res.status(200).json({ msg: "Contato salvo com sucesso!" });
});

app.get('/', (req, res) => {
  res.send('API Projeto Next - Filmes');
});

app.listen(port, () => {
  console.log(`Servidor Rodando na Porta: ${port}`);
});
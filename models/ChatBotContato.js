import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';

export const ChatBotContato = sequelize.define('ChatBotContato', {
  nome: {
    type: DataTypes.STRING,
    allowNull: false, // Nome é obrigatório
  },
  contato: {
    type: DataTypes.STRING,
    allowNull: false, // Contato (número de telefone) é obrigatório
  },
} 
  
);

import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Admin } from './Admin.js';

export const Filme = sequelize.define('filme', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  marca: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  modelo: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  cor: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  descricao: {
    type: DataTypes.STRING(500),
    allowNull: false
  },
  destaque: {
    type: DataTypes.BOOLEAN,
    defaultValue: false // Modifiquei para false, já que é um booleano
  },
  soma: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  num: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  total: {
    type: DataTypes.INTEGER(8),
    allowNull: false,
    defaultValue: 0
  },
  destaque: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: true
  },
  foto: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
}, {
  paranoid: true
});

Filme.belongsTo(Admin, {
  foreignKey: {
    name: 'admin_id',
    defaultValue: 1,    // para os filmes inicialmente cadastrados
    allowNull: false
  },
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
})

Admin.hasMany(Filme, {
  foreignKey: 'admin_id'
})


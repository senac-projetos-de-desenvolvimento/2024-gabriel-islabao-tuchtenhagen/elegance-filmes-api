import { useState } from 'react';

const Gallery = ({ images }) => {
  const [selectedImage, setSelectedImage] = useState(null);

  const openImage = (image) => {
    setSelectedImage(image);
  };

  const closeImage = () => {
    setSelectedImage(null);
  };

  return (
    <div className="flex flex-wrap -m-2">
      {images.map((image, index) => (
        <div key={index} className="p-2 w-1/2 md:w-1/3 lg:w-1/4">
          <img
            src={image.src}
            alt={image.alt}
            className="object-cover h-48 w-full cursor-pointer"
            onClick={() => openImage(image)}
          />
        </div>
      ))}

      {selectedImage && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-75 z-50">
          <button
            className="absolute top-4 right-4 text-white text-2xl"
            onClick={closeImage}
          >
            &times;
          </button>
          <img src={selectedImage.src} alt={selectedImage.alt} className="max-h-full max-w-full" />
        </div>
      )}
    </div>
  );
};

export default Gallery;
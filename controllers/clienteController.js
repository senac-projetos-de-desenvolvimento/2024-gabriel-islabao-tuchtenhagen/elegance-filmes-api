
import { Cliente } from '../models/Cliente.js'


export const clienteIndex = async (req, res) => {

  try {
    const clientes = await Cliente.findAll();
    res.status(200).json(clientes)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const clienteCreate = async (req, res) => {
  const { nome, email, senha } = req.body

  // se não informou estes atributos
  if (!nome || !email || !senha) {
    res.status(400).json({ id: 0, msg: "Erro... Informe os dados" })
    return
  }


  try {
    const cliente = await Cliente.create({ nome, email, senha });
    res.status(201).json(cliente)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const clienteLogin = async (req, res) => {

  const { email, senha } = req.body

  try {
    const cliente = await Cliente.findOne({ where: { email, senha } });

    if (cliente == null) {
      res.status(400).json({ erro: 'Login ou senha incorreto' })
      return
    }

      if(cliente){
      res.status(200).json({ id: cliente.id, nome: cliente.nome })
    }
    else {
      res.status(401).json({ erro: 'Login ou senha incorreto' })
      return
    }
  } catch (error) {
    res.status(400).send(error)
  }
}

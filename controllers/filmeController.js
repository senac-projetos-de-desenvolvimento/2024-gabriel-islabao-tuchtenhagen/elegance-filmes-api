import { Op } from "sequelize"
import { Filme } from '../models/Filme.js';

export const filmeIndex = async (req, res) => {
  try {
    const filmes = await Filme.findAll();
    res.status(200).json(filmes);
  } catch (error) {
    res.status(400).send(error);
  }
};

export const filmeDestaques = async (req, res) => {
  try {
    const filmes = await Filme.findAll({ where: { destaque: true } });
    res.status(200).json(filmes);
  } catch (error) {
    res.status(400).send(error);
  }
};

export const filmeDestaca = async (req, res) => {
  const { id } = req.params;

  try {
    const filme = await Filme.findByPk(id);
    await Filme.update({ destaque: !filme.destaque }, { where: { id } });
    res.status(200).json(filme);
  } catch (error) {
    res.status(400).send(error);
  }
};

export async function filmeCreate(req, res) {
  const { marca, modelo, cor, foto, descricao, destaque, admin_id } = req.body

  if (!marca || !modelo || !cor || !foto || !descricao || !admin_id) {
    res.status(400).json("Erro... Informe marca, modelo, cor, foto, ou descricao do filme")
    return
  }

  try {
    const filme = await Filme.create({
      marca, modelo, cor, foto, descricao, destaque, admin_id
    })
    res.status(201).json(filme)
  } catch (error) {
    res.status(400).send(error)
  }
}


export const filmeDestroy = async (req, res) => {
  const { id } = req.params;

  try {
    await Filme.destroy({ where: { id } });
    res.status(200).json({ msg: 'Ok! Removido com Sucesso' });
  } catch (error) {
    res.status(400).send(error);
  }
};

export async function filmeDelete(req, res) {

  const { id } = req.params

  try {
    await Filme.destroy({
      where: { id }
    })
    res.status(200).json({ msg: "Ok! Filme removido com sucesso" })
  } catch (error) {
    res.status(400).send(error)
  }
}


export const filmeShow = async (req, res) => {
  const { id } = req.params;

  try {
    const filme = await Filme.findByPk(id);
    res.status(200).json(filme);
  } catch (error) {
    res.status(400).send(error);
  }
};

export async function filmePesquisa(req, res) {
  const { palavra } = req.params

  try {
    const filme = await Filme.findAll({
      where: {
        marca: {
          [Op.like]: '%' + palavra + '%'
        }
      },
      order: [['id', 'desc']]
    })
    res.status(200).json(filme)
  } catch (error) {
    res.status(400).send(error)
  }
}


import { ChatBotContato } from '../models/ChatBotContato.js'


export const chatbotIndex = async (req, res) => {

  try {
    const usuarios = await ChatBotContato.findAll();
    res.status(200).json(usuarios)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const chatbotCreate = async (req, res) => {
  const { nome, contato } = req.body

  // se não informou estes atributos
  if (!nome || !contato) {
    res.status(400).json({ id: 0, msg: "Erro... Informe os dados" })
    return
  }


  try {
    const usuario = await ChatBotContato.create({ nome, contato });
    res.status(201).json(usuario)
  } catch (error) {
    res.status(400).send(error)
  }
}

